#!/usr/bin/python
# -*- coding: utf-8 -*-

# Gera config MRTG para chan_dongle
# E armazena outros dados no DB MySQL
#
# Sintaxe: dongle_stats.py <devices>
# Exemplo: dongle_stats.py ch01,ch02,ch03
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2014-09-05
#

import sys
import os
import socket
import subprocess
import time
import re
from datetime import datetime

import mysql.connector
from mysql.connector import errorcode

def UpdateDB(device,dictDevice,DB_host,DB_dbname,DB_user,DB_password):

	id_device = 0

	try:
		conn = mysql.connector.connect(user=DB_user,password=DB_password,host=DB_host,database=DB_dbname)

	except mysql.connector.Error as err:

		if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    			print("Something is wrong with your user name or password")
		elif err.errno == errorcode.ER_BAD_DB_ERROR:
			print("Database does not exists")
		else:
			print(err)
	else:

		conn.autocommit = True

		id_device=updateLastStatus(conn,device,dictDevice)

		if ( id_device > 0 ):

			insertHistory(conn,device,id_device,dictDevice)
		
		conn.close()

def updateLastStatus(conn,device,data):

	id_device=0
	id_device=searchLastStatusDevice(conn,device)

	if id_device == 0:

		if insertLastStatusDevice(conn,device,data) == 0:

			id_device=searchLastStatusDevice(conn,device)
			return id_device

	else:
		cursor_update = conn.cursor()

		sql_update = "UPDATE Last_status SET date=now(),rssi=%i,channel='%s',operadora='%s',imei='%s',modem='%s',number='%s',grupo=%i,state='%s',registrogsm='%s' WHERE id=%i;" % (int(data['rssi']),data['channel'],data['operadora'],data['imei'],data['modem'],data['number'],int(data['grupo']),data['state'],data['registrogsm'],id_device)

		try:
			cursor_update.execute(sql_update)

		except mysql.connector.Error as err:

			print (err)

			return 0
		else:

			cursor_update.close()

			return id_device


def insertLastStatusDevice(conn,device,data):
	
	cursor_insert = conn.cursor()

	sql_insert = "INSERT INTO Last_status (date,device,rssi,channel,operadora,imei,modem,number,grupo,state,registrogsm) VALUES (now(),'%s',%i,'%s','%s','%s','%s','%s',%i,'%s','%s');" % (device,int(data['rssi']),data['channel'],data['operadora'],data['imei'],data['modem'],data['number'],int(data['grupo']),data['state'],data['registrogsm'])

	try:
		cursor_insert.execute(sql_insert)

	except mysql.connector.Error as err:
	
		print (err)
		return 1
	else:

		cursor_insert.close()

		return 0


def searchLastStatusDevice(conn,device):

	id_device = 0

	cursor_search = conn.cursor()

	sql_search = "SELECT id from Last_status where device='%s';" % device

	try:
		cursor_search.execute(sql_search)

	except mysql.connector.Error as err:

		print (err)
	else:

		result = cursor_search.fetchone()

		if result is not None:

			id_device=result[0]

		cursor_search.close()

	return id_device	

def insertHistory(conn,device,id_device,data):

	cursor_insert = conn.cursor()

	sql_insert = "INSERT INTO Device_history (id_device,date,device,rssi,channel,operadora,imei,modem,number,registrogsm) VALUES (%i,now(),'%s',%i,'%s','%s','%s','%s','%s','%s');" % (id_device,device,int(data['rssi']),data['channel'],data['operadora'],data['imei'],data['modem'],data['number'],data['registrogsm'])

	try:
		cursor_insert.execute(sql_insert)

	except mysql.connector.Error as err:

		print (err)
		return 1
	else:

		return 0

def getdata(sck,cmd):

        sck.send(cmd + "\n")
        time.sleep (0.5)
        data = s.recv(65536)

        return data

def getAMIData(host,port,user,password):

        myConnect=connectAMI(host,port,user,password)
        s = myConnect.socket

	s.send('Action: DongleShowDevices\n\n')

        time.sleep (0.2)

        data = s.recv(65536)

        return data

def getDictDevice(data,device):

        mgrdictev=ManagerDictEvents(data,'Device',device)

	stats = {}
	for event in mgrdictev.events:

                modem_manufacturer = event['Manufacturer']
                modem_model = event['Model']
		operadora = event['ProviderName']
		imei = event['IMEISetting']
		number = event['SubscriberNumber']
		grupo = event['Group']
		state = event['State']
		registrogsm = event['GSMRegistrationStatus']

                stats.update({"rssi":event['RSSI'].split(',')[0]})
                stats.update({"channel":device})
                stats.update({"operadora":operadora.replace("\r","")})
                stats.update({"imei":imei.replace("\r","")})
                stats.update({"modem":modem_manufacturer.replace("\r","")+" - "+modem_model.replace("\r","")})
                stats.update({"number":number.replace("\r","")})
                stats.update({"grupo":grupo.replace("\r","")})
                stats.update({"state":state.replace("\r","")})
                stats.update({"registrogsm":registrogsm.replace("\r","")})

        return stats

class connectAMI:

        def __init__(self,host,port,user,password):

                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((host,port))

                s.send('Action: Login\nUsername: '+user+'\nSecret: '+password+'\n\n')
                time.sleep (0.5)
                data = s.recv(2048)

                self.socket = s

class ManagerDict:

        def __init__(self,text):

                # split the text
                linhas = text.split('\n')

                d={}

                for item in linhas:

                        dado=item.split(":",1)

                        try:

                                title=dado[0]
                        except:

                                title=""

                        try:

                                value=dado[1]
                        except:

                                value=""


                        d.update({title:value})

                self.dicti=d

        def getitem(self,index):

                i=self.dicti[index]
                i=i.replace(" ","\ ")
                return i


class ManagerDictEvents:

        def __init__(self,text,type,myevent):

                self.description = "Gera lista de dicionários dos itens de um evento específico."

                # split the text
                linhas = text.split('\n')

                i=0

                dicio={}
                events=[]

                fulldicio={}
                dicioitem=0

                if text.find(type + ': ' + myevent) == -1:

                        self.isvalid=False
                else:

                        self.isvalid=True

                        self.events=[]
                        self.items=0
                        self.fulldicti={}
                        self.dictiitems=0

                        for myitem in linhas:

                                dado=myitem.split(":",1)

                                try:

                                        title=dado[0]
                                except:

                                        title=""

                                try:
                                        value=dado[1]
                                except:

                                        value=""

                                if (title == type and value.find(myevent) > 0 and len(dicio) == 0):

                                        i+=1
                                        events.append({})
                                        dicioitem+=1
                                        fulldicio.update({title:value})

                                        dicio.update({title:value})
                                        events[i-1].update({title:value})

                                elif (title == type and len(dicio)>0):

                                        dicioitem+=1
                                        fulldicio.update({title:value})

                                        if len(dicio) > 0:

                                                events[i-1].update({title:value})

                                                dicio.clear()
                                                dicio={}

                                        if value.find(myevent) > 0:

                                                i+=1
                                                events.append({})
                                                dicioitem+=1
                                                fulldicio.update({title:value})

                                                events[i-1].update({title:value})
                                                dicio.update({title:value})

                                elif (title != "" and len(dicio)>0):

                                        dicioitem+=1
                                        fulldicio.update({title:value})

                                        events[i-1].update({title:value})
                                        dicio.update({title:value})

                        self.items=i
                        self.events=events
                        self.dicti=fulldicio
                        self.dictiitems=dicioitem
                        self.text=text


#
# Start script
#

AMI_host = '127.0.0.1'
AMI_port = 5038
AMI_user = 'dongle'
AMI_password = 'wQjPRFNw'

Data_Limite = "31/10/2014"
Data_Limite = datetime.strptime(Data_Limite, "%d/%m/%Y").date()

if Data_Limite < datetime.now().date():
	print "A ferramenta não pode mais ser utilizada pois passou de sua data limite que é %s." % datetime.strftime(Data_Limite,"%d/%m/%Y")
	sys.exit(1)

DB_host = '127.0.0.1'
DB_dbname = 'dongle_monitor'
DB_user = 'dongle_monitor'
DB_password = 'CSbBMT3j'

if (len(sys.argv) == 1):
	print "Sintaxe: dongle_stats.py <devices>"
	sys.exit(1)

# DEBUG
log = open('/var/log/dongle_stats.py.log', 'a')
sys.stderr = log
#sys.stdout = log

if (sys.argv[1] == "-d"):

	if (len(sys.argv) == 2):
		print "Sintaxe: dongle_stats.py -d <device>"
		sys.exit(1)

	device=sys.argv[2]
	data=getAMIData(AMI_host,AMI_port,AMI_user,AMI_password)
	dictDevice=getDictDevice(data,device)

	print "Device: %s" % device
	print "rssi: %s" % dictDevice['rssi']
	print "channel: %s" % dictDevice['channel']
	print "operadora: %s" % dictDevice['operadora']
	print "imei: %s" % dictDevice['imei']
	print "modem: %s" % dictDevice['modem']
	print "number: %s" % dictDevice['number']
	print "group: %s" % dictDevice['grupo']
	print "state: %s" % dictDevice['state']
	print "registrogsm: %s" % dictDevice['registrogsm']

else:

	devices=sys.argv[1].split(",")
	data=getAMIData(AMI_host,AMI_port,AMI_user,AMI_password)

	for device in devices:
		dictDevice=getDictDevice(data,device)
		UpdateDB(device,dictDevice,DB_host,DB_dbname,DB_user,DB_password)	

