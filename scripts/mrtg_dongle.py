#!/usr/bin/python
# -*- coding: utf-8 -*-

# Gera config MRTG para chan_dongle
# Pega dados do DB
#
# Sintaxe: mrtg_dongle.py <device>
# Exemplo: mrtg_dongle.py ch01
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2014-09-04
#
# Versao 0.1

import sys
import os
import socket
import subprocess
import time
import re
from datetime import datetime

import mysql.connector
from mysql.connector import errorcode


def printMRTG(rssi):

	if ( rssi == 30 ):
        	RSSI_Perc = 100
	else:
        	RSSI_Perc = rssi * 3.33

	print 0
	print str(RSSI_Perc)
	print 0
	print 0

def getRSSIDB(device,DB_host,DB_dbname,DB_user,DB_password):

	rssi=0

        try:
                conn = mysql.connector.connect(user=DB_user,password=DB_password,host=DB_host,database=DB_dbname)

        except mysql.connector.Error as err:

                if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                        print("Something is wrong with your user name or password")
                elif err.errno == errorcode.ER_BAD_DB_ERROR:
                        print("Database does not exists")
                else:
                        print(err)
        else:

		cursor = conn.cursor()

		sql = "SELECT rssi from Last_status WHERE device = '%s';" % device
		try:
			cursor.execute(sql)

        	except mysql.connector.Error as err:

			print (err)
		else:

			result = cursor.fetchone()

			if result is not None:

				rssi=result[0]

		conn.close()

	return rssi

DB_host = '127.0.0.1'
DB_dbname = 'dongle_monitor'
DB_user = 'dongle_monitor'
DB_password = 'CSbBMT3j'

Data_Limite = "31/10/2014"
Data_Limite = datetime.strptime(Data_Limite, "%d/%m/%Y").date()

if Data_Limite < datetime.now().date():
        print "A ferramenta não pode mais ser utilizada pois passou de sua data limite que é %s." % datetime.strftime(Data_Limite,"%d/%m/%Y")
        sys.exit(1)

if (len(sys.argv) == 1):
	print "Sintaxe: mrtg_dongle.py <device>"
	sys.exit(1)

device=sys.argv[1]

# DEBUG
log = open('/var/log/mrtg_dongle.py.log', 'a')
sys.stderr = log
#sys.stdout = log

rssi=getRSSIDB(device,DB_host,DB_dbname,DB_user,DB_password)
printMRTG(rssi)
